==== LWC Component ====

This is a custom LWC Component which provides an accordian type functionality to each table row. The data displayed currently is hard-coded. You can retrieve data from APEX class as well in a wrapper but the remaining functionality remains same.
This can be used as a prototype in different component wherever you want to place it.