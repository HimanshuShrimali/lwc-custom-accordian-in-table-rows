import { LightningElement, track } from 'lwc';

export default class App extends LightningElement {
    /**
     * @track indicates that if this object changes,
     * the UI should update to reflect those changes.
     */
    @track
    data = [
        {
            id: '1',
            name: 'ABC', 
            value1: 'val1',
            value2: 'val2',
            value3: 'val3',
            result: 'result ABC',
            expanded: false,
            childData: [
                {
                    id: '11',
                    name: 'child ABC',
                    value1: 'val11',
                    value2: 'val12',
                    value3: 'val13',
                    result: 'child result ABC'
                }
            ]
        },
        {
            id: '2',
            name: 'DEF', 
            value1: 'val1',
            value2: 'val2',
            value3: 'val3',
            result: 'result DEF',
            expanded: false,
            childData: [
                {
                    id: '21',
                    name: 'child DEF',
                    value1: 'val21',
                    value2: 'val22',
                    value3: 'val23',
                    result: 'child result DEF'
                }
            ]
        },
        {
            id: '3',
            name: 'XYZ', 
            value1: 'val1',
            value2: 'val2',
            value3: 'val3',
            result: 'result XYZ',
            expanded: false,
            childData: [
                {
                    id: '31',
                    name: 'child XYZ',
                    value1: 'val31',
                    value2: 'val32',
                    value3: 'val33',
                    result: 'child result XYZ'
                }
            ]
        }
    ];

    async toggleAccordian(event) {
        const val = event.currentTarget.value;
        let projectDataVal = this.data;
        const accordianBtn = this.template.querySelectorAll('lightning-button-icon');
        await accordianBtn.forEach(function(item) {
            if(item.value === val) {
                if(item.iconName === 'utility:chevrondown') {
                    item.iconName = 'utility:chevronright';
                    projectDataVal[item.value].expanded = false;
                }
                else if(item.iconName === 'utility:chevronright') {
                    item.iconName = 'utility:chevrondown';
                    projectDataVal[item.value].expanded = true;
                }
            }
        }); 
        
        this.data = projectDataVal;
    }
}
